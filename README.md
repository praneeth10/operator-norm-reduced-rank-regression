# Reduced Rank Regression in Operator Norm Error

This repository contains an implemntation of our Algorithm from the paper "Reduced Rank Regression in Operator Norm Error". Contact authors if you find any bugs.

## Usage
The function ``operator_norm_regression`` takes the following parameters:
- Sparse matrix $`A \in \R^{n \times c}`$ in CSR format 
- Sparse matrix $`B \in \R^{n \times d}`$ in CSR format
- An integer $`k \le \min{c,d}`$
- Accuracy parameter $`\varepsilon < 1`$
- Cost $`\beta \ge \text{Opt}`$

The function returns two matrices $`X \in \mathbb{R}^{c \times k}`$ and $`Y \in \mathbb{R}^{k \times d}`$ such that 
```math
\|A(X \cdot Y) - B\|_2 \le (1+O(\varepsilon))\beta.
```
